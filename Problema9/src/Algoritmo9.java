import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Algoritmo9 {

	public static void main(String[] args) throws IOException {
		System.out.println("Ingrese el numero de km ");
		BufferedReader dist = new BufferedReader(
				new InputStreamReader(System.in));

		String distancia_string = dist.readLine();
		double distancia = Integer.parseInt(distancia_string);
		
		if (distancia > 1000  ) {
			double valor=distancia *25;
			System.out.println("El valor a pagar es de "+ valor);
		} else {
			
			System.out.println("El valor a pagar es de "+ distancia* 45);
		}


	}

}
