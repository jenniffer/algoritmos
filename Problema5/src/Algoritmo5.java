import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Algoritmo5 {

	public static void main(String[] args) throws IOException {
		System.out.println("Ingrese el valor de la venta ");
		BufferedReader num = new BufferedReader(
				new InputStreamReader(System.in));
		String valor_string = num.readLine();
		double valor = Integer.parseInt(valor_string);

		if (valor >= 150000) {

			double iva = 16 * valor / 100;
			double precio = valor + iva;
			double descuento = valor * 25 / 100;
			double precioTotal=precio - descuento;
			System.out
					.println("El valor de la compra es " + precioTotal);

		}else {
			double iva = 16 * valor / 100;
			double precio = valor + iva;
			System.out
			.println("El valor de la compra es " + precio);
		}
			


	}

}
