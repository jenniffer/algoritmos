import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Algoritmo4 {

	public static void main(String[] args) throws IOException {
		System.out.println("Ingrese un numero ");
		BufferedReader num = new BufferedReader(
				new InputStreamReader(System.in));

		String numero_string = num.readLine();
		double numero = Integer.parseInt(numero_string);

		if (numero >= 10 ) {
			double numero1=numero * 3;
			System.out.println(numero1);
		} else {
			double numero1=numero/4;
			System.out.println(numero1);
		}
	}

}
