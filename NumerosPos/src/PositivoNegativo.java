import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PositivoNegativo {

	public static void main(String[] args) throws IOException {

		System.out.println("Ingrese un numero ");
		BufferedReader num = new BufferedReader(
				new InputStreamReader(System.in));

		String numero_string = num.readLine();
		int numero = Integer.parseInt(numero_string);

		if (numero > 0) {
			System.out.println("El numero es positivo.");
		} else {
			System.out.println("El numero es negativo.");
		}
	}

}
